$(() => {
    pridobiPodatke(prikazi);
});

function prikazi(data) {
    $('#seznamavtomobilov').empty();
    $.each(data, (index, avto) => {
        $('<li>', {
            html: `${avto.znamka}, ${avto.model}, ${avto.letnik}, ${avto.cena}€`,
            class: 'list-group-item'
        }).appendTo($('#seznamavtomobilov'));

    });
}

function pridobiPodatke(izpisi) {

    //JSONP
    //$.getJSON("http://localhost:4000/api/avtomobili?callback=?", izpisi);

    // $.ajax({
    //     type: "GET",
    //     url: "http://localhost:4000/api/avtomobili",
    //     dataType: "jsonp",
    //     success: (data) => {
    //         izpisi(data);
    //     },
    //     error: (err) => {
    //         alert(JSON.stringify(err));
    //     }
    // });


    //cors
    $.getJSON("http://localhost:4000/api/avtomobili", izpisi);


    // $.ajax({
    //     type: "GET",
    //     url: "http://localhost:4000/api/avtomobili",
    //     dataType: "json",
    //     success: (data) => {
    //         izpisi(data);
    //     },
    //     error: (err) => {
    //         alert(JSON.stringify(err));
    //     }
    // });
}

$('#nalozi').on('click', () => {
    pridobiPodatke(prikazi);
});

$('#poslji').on('click', () => {
    var avto = {
        znamka: $('#znamka').val(),
        model: $('#model').val(),
        letnik: $('#letnik').val(),
        cena: $('#cena').val(),
    };

    //alert(JSON.stringify(avto));
    $.ajax({
        type: "POST",
        url: "http://localhost:4000/api/avtomobili",
        data: avto,
        dataType: "json",
        success: (data) => {
            $('input[type=text]').val();
            pridobiPodatke(prikazi);
        },
        error: (err) => {
            alert(JSON.stringify({ 'Napaka': err }));
        }
    });
})