const moment = require('moment');

const mojLogger = function(req, res, next) {
    const zahteva = `${req.method} - ${req.protocol}://${req.get('host')}${req.originalUrl} - ${moment().format()}`;
    console.log(zahteva);
    next();
}

module.exports = mojLogger;