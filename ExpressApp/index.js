const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');

const avtomobili = require('./Avtomobili');
const avtomobiliRouter = require('./routes/api/avtomobili');

const uuid = require('uuid');
const mojlogger = require('./mojlogger');
const PORT = process.env.PORT || 4000;

const app = express();

app.engine('handlebars',
    exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');


app.use(mojlogger);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//app.use('/', express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => res.render('index', {
    naslov: 'Avtomobili-App',
    avtomobili: avtomobili
}));

app.get('/nov', (req, res) => res.render('nov', {
    naslov: 'Avtomobili-App'
}));

app.use('/api/avtomobili', avtomobiliRouter);


// app.get('/', (req, res) => {
//     res.sendFile(path.join(__dirname, 'public', 'index.html'));
// });

// app.get('/', (req, res) => {
//     res.send('GET');
// });

// app.post('/', (req, res) => {
//     res.send('POST');
// });

// app.put('/', (req, res) => {
//     res.send('PUT');
// });

// app.delete('/', (req, res) => {
//     res.send('DELETE');
// });

// app.get('/', (req, res) => {
//     res.send('<h1>Hej!</h1><h2>To je prvi Express app</h2>');
// });


app.listen(PORT, () => {
    console.log(`Strežnik posluša na http://localhost:${PORT}`);
});