const express = require('express');
const cors = require('cors');
const uuid = require('uuid');
const avtomobili = require('../../Avtomobili');

const router = express.Router();

router.use(cors());

router.get('/', (req, res) => {
    res.json(avtomobili);
});

router.get('/:id', (req, res) => {

    const poisci = avtomobili.some(avto => avto.id === req.params.id);
    if (poisci) {
        const vrni = avtomobili.filter(avto => avto.id === req.params.id)
        res.json(vrni);
    } else {
        res.sendStatus(404).json({ msg: `avto z id=${req.params.id} ni v bazi` });
    }

});

router.post('/', (req, res) => {
    const novAvto = {
        id: uuid.v4(),
        znamka: req.body.znamka,
        model: req.body.model,
        letnik: req.body.letnik,
        cena: req.body.cena
    };
    if (!novAvto.znamka || !novAvto.model || !novAvto.letnik || !novAvto.cena) {
        res.status(400).json({ msg: 'Poslati je potrebno podatke' })
    } else {
        avtomobili.push(novAvto);
        res.location(`${req.protocol}://${req.get('host')}${req.originalUrl}/${novAvto.id}`)
            .json(novAvto);
        //res.redirect('/');
    }
})

router.put('/:id', (req, res) => {
    if (req.params.id !== req.body.id) {
        res.status(400).json({ msg: 'Id v URI ni enak id-ju v objektu' });
    } else {
        const najden = avtomobili.some(avto => avto.id == req.params.id);
        if (najden) {
            const updateAvto = req.body;
            avtomobili.forEach(avto => {
                if (avto.id == req.body.id) {
                    avto.znamka = updateAvto.znamka ? updateAvto.znamka : avto.znamka;
                    avto.model = updateAvto.model ? updateAvto.model : avto.model;
                    avto.letoIzdelave = updateAvto.letoIzdelave ? updateAvto.letoIzdelave :
                        avto.letoIzdelave;
                    avto.cena = updateAvto.cena ? updateAvto.cena : avto.cena;
                    res.json({ msg: 'Avto posodobljen', avto });
                }
            });
            res.json(updateAvto);
        } else {
            res.status(404).json({ msg: `Avto z id=${req.params.id} ni najden` });
        }
    }
});

router.delete('/:id', (req, res) => {
    const najden = avtomobili.some(avto => avto.id == req.params.id);
    if (najden) {
        res.json({
            msg: 'Avto zbrisan',
            avtomobili: avtomobili.filter(avto => avto.id != req.params.id)
        });
    } else {
        res.status(404).json({ msg: `Avto z id=${req.params.id} ni najden` });
    }
});

module.exports = router;