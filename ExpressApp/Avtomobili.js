const uuid = require('uuid');

const avtomobili = [{
        id: uuid.v4(),
        znamka: 'Audi',
        model: 'A4',
        letnik: 2018,
        cena: 33000
    },
    {
        id: uuid.v4(),
        znamka: 'Mazda',
        model: 'CX5',
        letnik: 2018,
        cena: 23000
    },
    {
        id: uuid.v4(),
        znamka: 'Volvo',
        model: 'XC90',
        letnik: 2018,
        cena: 53000
    }
]

module.exports = avtomobili;